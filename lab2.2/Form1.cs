﻿using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace lab2._2
{
    public partial class Form1 : Form
    {
        private readonly Point[] _points;
        private int _lastIndex;
        public Form1()
        {
            InitializeComponent();
            MouseClick += Form1_Click; 
            _points = new Point[3];
        }
        private void Form1_Click(object sender, MouseEventArgs e)
        {
            _points[_lastIndex] = new Point(e.X, e.Y);
            _lastIndex++;
            if (_lastIndex == 3)
                _lastIndex = 0;
            
            if (_points.Count(n => n != Point.Empty) != 3) return;
            CreateGraphics().Clear(SystemColors.Control);
            CreateGraphics().DrawPolygon(new Pen(Color.Black, 3), _points);
        }
    }
}